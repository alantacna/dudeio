import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {

  validation_messages = {
    'fullname': [
      { type: 'required', message: 'Full Name is required'},
      { type: 'pattern', message: 'Your Full name must contain only letters'}
    ],
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number.' }
    ],
    'confirm_password': [
      { type: 'required', message: 'Confirm password is required.' }
    ],
    'matching_passwords': [
      { type: 'areEqual', message: 'Password mismatch.' }
    ],
    'address': [
      { type: 'required', message: 'Address is required.'}
    ],
    'city': [
      { type: 'required', message: 'City field is required.'},
      { type: 'pattern', message: 'City name must contain only letters'}
    ],
  };

  validations_form: FormGroup;
  matching_passwords_group: FormGroup;

  months: Array<string>;
  days: Array<number>;
  years: Array<number>;

  states: Array<string>;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router
  ) { }

  ngOnInit() {
    this.months = [
      'January', 'February', 'March', 'April', 'May', 'June', 
      'July', 'August', 'September', 'October', 'November', 'December'
    ];

    this.days = [
      1, 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10,
      11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30
    ];

    this.years = [1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990];

    this.states = ['California', 'Arizona', 'Nevada'];

    this.matching_passwords_group = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      confirm_password: new FormControl('', Validators.required)
    }, (formGroup: FormGroup) => {
      return PasswordValidator.areEqual(formGroup);
    });

    this.validations_form = this.formBuilder.group({
      fullname: new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z\\s]*$'),
        Validators.required
      ])),
      month: new FormControl(this.months[0], Validators.required),
      day: new FormControl(this.days[0], Validators.required),
      year: new FormControl(this.years[0], Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      matching_passwords: this.matching_passwords_group,
      address: new FormControl('', Validators.compose([
        Validators.required
      ])),
      city: new FormControl('', Validators.compose([
        Validators.pattern('^[a-zA-Z\\s]*$'),
        Validators.required
      ])),
      state: new FormControl(this.states[0], Validators.required),
    });
  }

  onSubmit(values){
    console.log(values);
    alert('Thank you for your data');
    // this.router.navigate(["/user"]);
  }

}
